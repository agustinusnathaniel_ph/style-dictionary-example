# Style Dictionary Example

## Pre-requisites

- Node.js & npm / yarn installed
- `style-dictionary` installed

## Repo Init Steps

1. mkdir `style-dictionary-example`
2. cd `style-dictionary-example`
3. `npm i -g style-dictionary`
  (alternative: `npm i --save-dev style-dictionary` or `yarn add -D style-dictionary`)
4. `style-dictionary init basic`
  https://amzn.github.io/style-dictionary/#/quick_start

## Adding flutter platform configuration

1. Open `config.json`
2. Add under `platforms`:
```json
// config.json

{
  ...,
  "platforms": {
    ...,
    "flutter": {
      "transformGroup": "flutter",
      "buildPath": "build/flutter/",
      "files": [{
        "destination": "style_dictionary.dart",
        "format": "flutter/class.dart"
      }]
    },
  }
}
```
  - https://amzn.github.io/style-dictionary/#/config?id=configuration
  - https://amzn.github.io/style-dictionary/#/transforms?id=transforms
  - https://amzn.github.io/style-dictionary/#/transform_groups
  - https://amzn.github.io/style-dictionary/#/transform_groups?id=flutter
  - https://amzn.github.io/style-dictionary/#/formats?id=formats
  - https://amzn.github.io/style-dictionary/#/formats?id=flutterclassdart
  - https://stackoverflow.com/a/69285743
3. Automatically build flutter style-dictionary class using this command:

```bash
// Flutter only
style-dictionary build -p flutter
```
```bash
// all platform
style-dictionary build
```