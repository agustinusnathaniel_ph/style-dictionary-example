
// StyleDictionarySize.h
//

// Do not edit directly
// Generated on Mon, 17 Jan 2022 03:33:48 GMT


#import <Foundation/Foundation.h>


extern float const SizeFontSmall;
extern float const SizeFontMedium;
extern float const SizeFontLarge;
extern float const SizeFontBase;
